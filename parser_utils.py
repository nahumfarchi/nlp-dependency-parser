#! /usr/bin/python

from collections import defaultdict, namedtuple
import itertools
from enum import Enum
import timeit
import numpy as np
from scipy import sparse

__author__ = "Igor Margulis, Nahum Farchi"

"""
Utility functions for DepParser
"""

PRINT_EPOCH = 1000

Word = namedtuple('Word', ['index', 'token', 'pos', 'head', 'child'])


def flatten_list(l):
    return list(itertools.chain.from_iterable(l))


def print_sentences(file, sentences):
    lines = []
    for sent in sentences:
        lines.append(['{}\t{}\t_\t{}\t_\t_\t{}\t_\t_\t_\n'\
                     .format(w.index, w.token, w.pos, w.head) for w in sent])

        lines[-1][-1] += '\n'
    file.writelines(flatten_list(lines))


def sentence_iterator(corpus_file, labeled=True):
    """
    Return an iterator object that yields one sentence at a time.
    Sentence is represented as a list of named tuples of the form
    (index, word, pos, parent, child) for every word.
    The validity of input is assumed.
    If we iterate over unlabeled data just split the sentence and
    yield a list of tuples with the available data.
    """

    l = corpus_file.readline()
    while l:
        sentence = [Word(0, 'ROOT', 'ROOT', None, None)]
        line = l.strip()
        while line:
            word_w_attributes = line.split("\t")
            if labeled:
                w = Word(int(word_w_attributes[0]),
                         word_w_attributes[1],
                         word_w_attributes[3],
                         int(word_w_attributes[6]),
                         None)
            else:
                w = Word(int(word_w_attributes[0]),
                         word_w_attributes[1],
                         word_w_attributes[3],
                         word_w_attributes[6],
                         None)
            sentence.append(w)
            line = corpus_file.readline().strip()
        l = corpus_file.readline()

        # now we have access to all words of the sentence.
        # so let's record the modifiers
        if labeled:
            for word in sentence:
                if word.head is not None:
                    sentence[word.head] = sentence[word.head]._replace(child=word.index)

        yield sentence


def compare_graphs_count(gold, predicted):
    A = set(((src, dst) for src in predicted for dst in predicted[src]))
    B = set(((src, dst) for src in gold for dst in gold[src]))
    return len(B.difference(A))


def compare_graphs(gold, predicted):
    for src in gold:
        for dst in gold[src]:
            if src not in predicted:
                return False
            elif dst not in predicted[src]:
                return False
    return True


def get_edges(n):
    return itertools.chain(((0, dst) for dst in xrange(1, n)),
                           ((src, dst) for src in xrange(1, n)
                            for dst in xrange(1, n) if src != dst))


class GlmFeatExtractor(object):
    """
        This class perform the feature extraction given the training corpus.
        In addition the class is responsible for the embedding.

        The class maintains the featuresets together with their counts.
        Featuresets are dictionaries containing the features per each dependency.
        The extraction is aimed to realize the first-order approach.
    """

    def __init__(self, training_sents, basic, rare_feat_cutoff):
        self.train_sents = training_sents
        self._basic = basic
        self._rare_feat_cutoff = rare_feat_cutoff
        self._mapping = self._build_mapping(training_sents)
        self._dim = len(self._mapping)

    def _build_mapping(self, training_sents):
        mapping = {}  # maps (fname, fval, label) -> fid
        mapping_counts = defaultdict(int)

        for sent in training_sents:
            n = len(sent)
            tree = self.build_tree(sent)
            for src in tree:
                for dst in tree[src]:
                    feature_set = self.extract_edge_feats(sent, src, dst)
                    for fname, fval in feature_set:
                        if (fname, fval) not in mapping:
                            mapping[fname, fval] = len(mapping)
                        mapping_counts[fname, fval] += 1

        if not self._basic and self._rare_feat_cutoff > 0:
            fixed_mapping = {}
            for f in mapping:
                if mapping_counts[f] > self._rare_feat_cutoff:
                    fixed_mapping[f] = len(fixed_mapping)
            mapping = fixed_mapping

        return mapping

    def encode(self, feature_set):
        encoding = []

        # Convert features
        for fname, fval in feature_set:
            # feature name & value:
            if (fname, fval) in self._mapping:
                encoding.append(self._mapping[fname, fval])

        return encoding

    def dim(self):
        return self._dim

    def build_pos_table(self, sent):
        """
        Prepare POS tables for in-between features

        table[i,j] - the set of all pos tags in between word_i and word_j
        is possible in O(n^2) using DP
        """
        n = len(sent)
        pos_table = {}
        for seq_len in xrange(2, n):
            for i in xrange(n-seq_len):
                j = i + seq_len
                if i in pos_table:
                    pos_table[i][j] = pos_table[i][j-1].union([sent[j-1].pos])
                else:
                    pos_table[i] = {j : set([sent[j-1].pos])}
        return pos_table

    def build_pos_tables(self, sentences):
        pos_tables = []
        for sent in sentences:
            pos_tables.append(self.build_pos_table(sent))
        return pos_tables

    def build_feature_matrix(self, sentences, verbose=False):
        """
        Create feature matrix of self.dim() dimensional space.
        Each row is a feature vector. Use with create_graphs
        and create_trees. They will return a graph/tree that
        points to the relevant row by g[src][dst] -> row number
        For example:
            mat = fe.build_feature_matrix(sentences)
            graphs = fe.build_graphs(sentences)
            g = graphs[0]
            mat[g[0][5], :] # feature vector of edge 0->5 in g
        """
        if verbose:
            print("Building feature matrix...")
            t_start = timeit.default_timer()

        if not self._basic:
            pos_tables = self.build_pos_tables(sentences)

        cols = []
        rows = []
        r = 0
        row_counter = itertools.count()
        pos_table = None
        for i, sent in enumerate(sentences):
            if verbose and i % 100 == 0:
                print('i: {}'.format(i))

            n = len(sentences[i])
            edges = get_edges(n)

            if not self._basic:
                pos_table = pos_tables[i]

            g_cols = [self.encode(self.extract_edge_feats(sentences[i], src, dst, pos_table)) for src, dst in edges]  # example: [[1],[1,3]]
            g_rows = np.array(flatten_list([[next(row_counter)] * len(c) for c in g_cols]), dtype=np.int)
            g_cols = np.array(flatten_list(g_cols), dtype=np.int).flatten()

            cols.append(g_cols)
            rows.append(g_rows)

        rows = np.concatenate(rows)
        cols = np.concatenate(cols)
        vals = np.ones(len(rows))

        n_rows = next(row_counter)
        complete_feature_mat = sparse.csr_matrix((vals, (rows, cols)), shape=(n_rows, self.dim()), dtype=np.float64)

        if verbose:
            print('time: {}'.format((timeit.default_timer()-t_start) / 60))

        return complete_feature_mat

    def build_tree(self, sent):
        tree = {}
        for word in sent[1:]:
            src, dst = word.head, word.index
            if src in tree:
                tree[src][dst] = None
            else:
                tree[src] = {dst: None}
        return tree

    def build_trees(self, sentences, verbose=False):
        """
        Create a list of trees from the given sentences.

        Each such tree maps each edge g[src][dst] to a feature vector
        row number.
        !!! Works only for labeled data. !!!
        """
        if verbose:
            print('Building trees...')
            t_start = timeit.default_timer()

        trees = []
        row_counter = itertools.count()
        for i, sent in enumerate(sentences):
            if verbose and i % PRINT_EPOCH == 0:
                print('i: {}'.format(i))

            n = len(sent)
            edges = get_edges(n)

            t = self.build_tree(sent)
            for src, dst in edges:
                row = next(row_counter)
                if src in t and dst in t[src]:
                    t[src][dst] = row
            trees.append(t)

        if verbose:
            print('Time: {}'.format((timeit.default_timer()-t_start) / 60))

        return trees

    def build_graphs(self, sentences, verbose=False):
        """
        Create a list of complete graphs.

        Each such graph maps each edge g[src][dst] to a feature vector row number.
        Can be used on unlabeled data.
        """
        if verbose:
            print('Building graphs...')
            t_start = timeit.default_timer()

        graphs = []
        row_counter = itertools.count()
        for i, sent in enumerate(sentences):
            if verbose and i % PRINT_EPOCH == 0:
                print('i: {}'.format(i))
            n = len(sent)
            edges = get_edges(n)
            g = {}
            for src, dst in edges:
                row = next(row_counter)
                if src in g:
                    g[src][dst] = row
                else:
                    g[src] = {dst : row}
            graphs.append(g)

        if verbose:
            print('time: {}'.format((timeit.default_timer()-t_start) / 60))

        return graphs

    Features = Enum('Features',
                    """ p_word_p_pos
                        p_word
                        p_pos
                        c_word_c_pos
                        c_word
                        c_pos
                        p_pos_c_pos
                        p_word_p_pos_c_pos
                        p_pos_c_word_c_pos
                        p_word_p_pos_c_word_c_pos
                        p_word_c_word_c_pos
                        p_word_p_pos_c_word
                        p_word_c_word
                        p_pos_p_right_pos_c_left_pos_c_pos
                        p_left_pos_p_pos_c_left_pos_c_pos
                        p_pos_p_right_pos_c_pos_c_right_pos
                        p_left_pos_p_pos_c_pos_c_right_pos
                        p_pos_b_pos_c_pos

                        p_word_p_pos_conj
                        p_word_conj
                        p_pos_conj
                        c_word_c_pos_conj
                        c_word_conj
                        c_pos_conj
                        p_pos_c_pos_conj
                        p_word_p_pos_c_pos_conj
                        p_pos_c_word_c_pos_conj
                        p_word_p_pos_c_word_c_pos_conj
                        p_word_c_word_c_pos_conj
                        p_word_p_pos_c_word_conj
                        p_word_c_word_conj
                        p_pos_p_right_pos_c_left_pos_c_pos_conj
                        p_left_pos_p_pos_c_left_pos_c_pos_conj
                        p_pos_p_right_pos_c_pos_c_right_pos_conj
                        p_left_pos_p_pos_c_pos_c_right_pos_conj
                        p_pos_b_pos_c_pos_conj
                        conj
                        """)

    def _basic_feature_set(self, sentence, src, dst):
        parent = sentence[src]
        child = sentence[dst]
        feature_set = [(self.Features.p_word, parent.token),
                       (self.Features.p_pos, parent.pos),
                       (self.Features.p_word_p_pos, (parent.token, parent.pos)),
                       (self.Features.c_word, child.token),
                       (self.Features.c_pos, child.pos),
                       (self.Features.c_word_c_pos, (child.token, child.pos)),
                       (self.Features.p_pos_c_pos, (parent.pos, child.pos)),
                       (self.Features.p_pos_c_word_c_pos, (parent.pos, child.token, child.pos))]
        return feature_set

    def extract_edge_feats(self, sentence, src, dst, pos_table=None):
        """
        Generates featuresets for edge (src,dst) in the given sentence.

        basic features:
            p-word p-pos
            p-word
            p-pos
            c-word c-pos
            c-word
            c-pos
            p-pos c-pos
            p-word p-pos c-pos
            p-pos c-word c-pos

        :param sentence:
        :param src:
        :param dst:
        :param pos_table:
        :return: A dictionary that maps feature names to feature values: fname -> fval.
        """
        if self._basic:
            return self._basic_feature_set(sentence, src, dst)
        else:
            parent = sentence[src]
            child = sentence[dst]
            if src > 1:
                parent_left = sentence[src-1]
            else:
                parent_left = None
            if src < len(sentence)-1:
                parent_right = sentence[src+1]
            else:
                parent_right = None
            if dst > 1:
                child_left = sentence[dst-1]
            else:
                child_left = None
            if dst < len(sentence)-1:
                child_right = sentence[dst+1]
            else:
                child_right = None
            if src > dst:
                direction = 'right'
            else:
                direction = 'left'
            distance = min(abs(src-dst), 5)
            feature_set = [
                # p-word p-token
                (self.Features.p_word, parent.token),
                # p-pos
                (self.Features.p_pos, parent.pos),
                # p-word p-pos
                (self.Features.p_word_p_pos, (parent.token, parent.pos)),
                # c-word
                (self.Features.c_word, child.token),
                # c-pos
                (self.Features.c_pos, child.pos),
                # c-word c-pos
                (self.Features.c_word_c_pos, (child.token, child.pos)),
                # p-pos c-pos
                (self.Features.p_pos_c_pos, (parent.pos, child.pos)),
                # p-pos c-word c-pos
                (self.Features.p_pos_c_word_c_pos, (parent.pos, child.token, child.pos)),
                # p-word p-pos c-word c-pos
                (self.Features.p_word_p_pos_c_word_c_pos, (parent.token, parent.pos, child.token, child.pos)),
                # p-word c-word c-pos
                (self.Features.p_word_c_word_c_pos, (parent.token, child.token, child.pos)),
                # p-word p-pos c-word
                (self.Features.p_word_p_pos_c_word, (parent.token, parent.pos, child.token)),
                # p-word c-word
                (self.Features.p_word_c_word, (parent.token, child.token)),

                # direction and distance
                (self.Features.conj, (direction, distance)),

                # same as above but with direction and distance
                (self.Features.p_word_conj, (parent.token, direction, distance)),
                (self.Features.p_pos_conj, (parent.pos, direction, distance)),
                (self.Features.p_word_p_pos_conj, (parent.token, parent.pos, direction, distance)),
                (self.Features.c_word_conj, (child.token, direction, distance)),
                (self.Features.c_pos_conj, (child.pos, direction, distance)),
                (self.Features.c_word_c_pos_conj, (child.token, child.pos, direction, distance)),
                (self.Features.p_pos_c_pos_conj, (parent.pos, child.pos, direction, distance)),
                (self.Features.p_pos_c_word_c_pos_conj, (parent.pos, child.token, child.pos, direction, distance)),
                (self.Features.p_word_p_pos_c_word_c_pos_conj, (parent.token, parent.pos, child.token, child.pos, direction, distance)),
                (self.Features.p_word_c_word_c_pos_conj, (parent.token, child.token, child.pos, direction, distance)),
                (self.Features.p_word_p_pos_c_word_conj, (parent.token, parent.pos, child.token, direction, distance)),
                (self.Features.p_word_c_word_conj, (parent.token, child.token, direction, distance))
                ]
            # p-pos p-right-pos c-left-pos c-pos
            if parent_right and child_left:
                feature_set.append((self.Features.p_pos_p_right_pos_c_left_pos_c_pos, (parent.pos, parent_right.pos, child_left.pos, child.pos)))
                feature_set.append((self.Features.p_pos_p_right_pos_c_left_pos_c_pos_conj, (parent.pos, parent_right.pos, child_left.pos, child.pos, direction, distance)))
            # p-left-pos p-pos c-left-pos c-pos
            if parent_left and child_left:
                feature_set.append((self.Features.p_left_pos_p_pos_c_left_pos_c_pos, (parent_left.pos, parent.pos, child_left.pos, child.pos)))
                feature_set.append((self.Features.p_left_pos_p_pos_c_left_pos_c_pos_conj, (parent_left.pos, parent.pos, child_left.pos, child.pos, direction, distance)))
            # p-pos p-right-pos c-pos c-right-pos
            if parent_right and child_right:
                feature_set.append((self.Features.p_pos_p_right_pos_c_pos_c_right_pos, (parent.pos, parent_right.pos, child.pos, child_right.pos)))
                feature_set.append((self.Features.p_pos_p_right_pos_c_pos_c_right_pos_conj, (parent.pos, parent_right.pos, child.pos, child_right.pos, direction, distance)))
            # p-left-pos p-pos c-pos c-rigth-pos
            if parent_left and child_right:
                feature_set.append((self.Features.p_left_pos_p_pos_c_pos_c_right_pos, (parent_left.pos, parent.pos, child.pos, child_right.pos)))
                feature_set.append((self.Features.p_left_pos_p_pos_c_pos_c_right_pos_conj, (parent_left.pos, parent.pos, child.pos, child_right.pos, direction, distance)))
            # inbetween pos
            if pos_table and distance > 1:
                for pos in pos_table[min(src,dst)][max(src,dst)]:
                    feature_set.append((self.Features.p_pos_b_pos_c_pos, (parent.pos, pos, child.pos)))
                    feature_set.append((self.Features.p_pos_b_pos_c_pos_conj, (parent.pos, pos, child.pos, direction, distance)))

            return feature_set

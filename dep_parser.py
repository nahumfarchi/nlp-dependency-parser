#! /usr/bin/python

import sys
import getopt
import time
from datetime import datetime
import parser_utils
import glm
import edmonds_fast

__author__ = "Igor Margulis, Nahum Farchi"

"""
A dependency parser which uses Global Linear Model (GLM)
to find the most likely dependency tree for a given sentence (word sequence).
The parser is trained on a corpus of labeled sentences.

For every dependency in the corpus, a tuple consisting of a dictionary of features extracted from
the dependency's context e.g. parent/child pos and words, and other relationships are generated.

The model, namely the parameter vector associated with the model, is learned
from these tuples. The model then is be used by the parser to find the most likely dependency
tree for any given word sequence.
The parser and the feature sets chosen for model are implemented in the spirit
of McDonald paper.

There are two modes, basic and advance. in the basic mode a restricted set of features is used.
"""


class DepParser(object):
    """
    DepParser is a dependency parser.
    """
    def __init__(self, in_basic=True, in_verbose=False):
        """
        Construct a new parser.

        @type in_basic: bool
        @param in_basic: If set to True we only use a basic set of features,
                         otherwise we use additional (more elaborated) features

        @type in_verbose: bool
        @param in_verbose: print out stuff
        """
        self.basic = in_basic
        self.verbose = in_verbose
        self.model = None

    def train(self, training_sents, n_epochs = 10, feat_cutoff = 3):
        """
        trains GLM from a list of labeled sentences.
        The algorithm that is used for the obtaining the
        best weight vector is the structured perceptron.

        @type training_sents: list(list(Word))
        @param training_sents: A list of labeled sentences. Each sentence is
        represented by a list of words. Each word is a named tuple with fields parent, child, token, pos.

        @type n_epochs: int
        @param n_epochs: number of epochs used by the perceptron.

        @type feat_cutoff: int
        @param feat_cutoff: features occurring less than
        feat_cutoff times during training are ignored.
        """

        if self.verbose:
            t_start = time.time()
            print ("Start time: {}".format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
            
        self.model = glm.Glm.train(training_sents, feat_cutoff, self.basic, n_epochs, self.verbose)

        if self.verbose:
            t_end = time.time()
            print("Training time in minutes: {}".format(round(t_end - t_start, 3)/60.))

    def label_sents(self, corpus):
        """
        Labels sentences.

        For each sentence and for each Word from a corresponding list the dependecy
        data is fed.

        @type corpus: list(list(Word))
        @param corpus: A list of unlabeled sentences. Each sentence is
        represented by a list of Words. Each word is a named tuple with fields parent, child, token, pos.

        @rtype : list(list(Word))
        @param : A list of labeled sentences. Each sentence is
        represented by a list of Words. Each word is a named tuple with fields parent, child, token, pos
        """
        feature_matrix = self.model.feat_extractor().build_feature_matrix(corpus)
        graphs = self.model.feat_extractor().build_graphs(corpus)

        n_words = 0
        for sent in corpus:
            n_words += len(sent)

        pred_trees = []
        for i, g in enumerate(graphs):
            pred_trees.append(glm.predict(feature_matrix, g, self.model.weights()))

        for i, sent in enumerate(corpus):
            t = pred_trees[i]
            rt = edmonds_fast._reverse(t)
            for src in rt:
                #sent[src].head = rt[src]

                sent[src] = sent[src]._replace(head=rt[src].keys()[0])
                #sentence[word.head]._replace(child=word.index)
        #return [self.label(sent) for sent in sentences]

    def evaluate(self, corpus):
        """
        Compare the accuracy of the parser against the given labeled corpus.
        First, strip the labeling from the corpus text, then label the text
        using the dependencies obtained from the parser,
        and finally compute the accuracy percentage.

        @type corpus: list(list(Word)
        @param corpus: The list of labeled sentences to score the parser on.

        @rtype: float
        """
        # TODO: rework
        feature_matrix = self.model.feat_extractor().build_feature_matrix(corpus)
        graphs = self.model.feat_extractor().build_graphs(corpus)
        trees = self.model.feat_extractor().build_trees(corpus)

        n_words = 0
        for sent in corpus:
            n_words += len(sent)

        acc = 0.0
        for i, g in enumerate(graphs):
            pred_tree = glm.predict(feature_matrix, g, self.model.weights())
            gold_tree = trees[i]
            acc += parser_utils.compare_graphs_count(gold_tree, pred_tree)
        return 1.0 - acc / n_words

def train_n_test(in_basic,
                 in_verbose,
                 corpus_file,
                 test_corpus_file,
                 rare_feature_cutoff=3,
                 n_epochs=5,
                 comp_in_file=None,
                 comp_out_file=None):
    """
    Reads the file containing the labeled corpus,
    trains parser and tests the parser accuracy.
    Labels unseen sentences from unseen corpus, if provided.

    @type in_basic: bool
    @param in_basic: use basic model

    @type in_verbose: bool
    @param in_verbose: print out stuff

    @type corpus_file: file
    @param corpus_file: the corpus file.

    @type test_corpus_file: file
    @param test_corpus_file: the test corpus file.

    @type unseen_file: file
    @param unseen_file: the file with unseen
    (and not tagged data).
    """

    s_iterator = parser_utils.sentence_iterator(corpus_file)
    labeled_sents = [s for s in s_iterator]

    parser = DepParser(in_basic, in_verbose)

    # the actual training happens here
    parser.train(labeled_sents, n_epochs, rare_feature_cutoff)

    # now we are ready to evaluate how well we perform
    s_iterator = parser_utils.sentence_iterator(test_corpus_file)
    test_sents = [s for s in s_iterator]

    if in_verbose:
        t_start = time.time()
        print ("==> Evaluation ===> started at: {}".format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))

    accuracy = parser.evaluate(test_sents)

    if in_verbose:
        t_end = time.time()
        print ("==> Evaluation ===> ended at: {}".format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        print("Evaluation time in minutes: {}".format(round(t_end - t_start, 3) / 60.))
        print ("==> Evaluation ===> Parser accuracy {}%\n".format(100*accuracy))
        print ("\n\n")

    # and finally let's face a real challenge
    if comp_out_file and comp_in_file:
        if in_verbose:
            t_start = time.time()
            print ("==> Inference ===> labeling unseen sentences...")
            print ("==> Inference ===> started at: {}".format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        s_iterator = parser_utils.sentence_iterator(comp_in_file, False)
        comp_sents = [s for s in s_iterator]
        parser.label_sents(comp_sents)
        parser_utils.print_sentences(comp_out_file, [[w for w in sent if w.token != 'ROOT'] for sent in comp_sents])

        if in_verbose:
            t_end = time.time()
            print("==> Inference ===> ended at: {}".format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
            print("==> Inference time in minutes: {}".format(round(t_end - t_start, 3) / 60.))
            print("==> Inference ===> Done with unseen sentences.")


def usage():
    print """
    python dep_parser.py  [-bv] -i <training.file> -t <test.file> [-c <input.file>]

    Train the Dependency Parser and test it.
    -h               -- display help
    -b               -- use the basic model
    -v               -- verbose
    -i training.file -- path to the file containing training data
    -t test.file     -- path to the file containing test data
    -c input.file    --  path to the file containing unlabeled data
    -n n_epochs      -- number of perceptron epochs
    -o out.file      -- output file of unlabled labeling
    """


if __name__ == "__main__":

    if len(sys.argv) < 2:  # at least one argument is expected
        usage()
        sys.exit(2)

    input_file_name = ''
    test_file_name = ''
    comp_in_file_name = ''
    comp_out_file_name = ''
    basic = False
    verbose = False
    n_epochs = 5
    rare_feature_cutoff = 0
    out = None

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hbvi:t:c:n:r:o:")
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt == '-b':
            basic = True
        elif opt == '-v':
            verbose = True
        elif opt == '-i':
            input_file_name = arg
        elif opt == '-t':
            test_file_name = arg
        elif opt == '-c':
            comp_in_file_name = arg
        elif opt == '-n':
            n_epochs = int(arg)
        elif opt == '-r':
            rare_feature_cutoff = int(arg)
        elif opt == '-o':
            comp_out_file_name = arg

    if not(test_file_name and input_file_name):
        usage()
        sys.exit(2)
    try:
        input_file = file(input_file_name, "r")
        test_file = file(test_file_name, "r")
        if comp_in_file_name:
            comp_in_file = file(comp_in_file_name, "r")
        else:
            comp_in_file = None
        if comp_out_file_name:
            comp_out_file = file(comp_out_file_name, 'w')
        else:
            comp_out_file = None


    except IOError:
        sys.stderr.write("ERROR: Cannot read file.\n")
        sys.exit(1)

    print("Started\n")

    train_n_test(basic,
                 verbose,
                 input_file,
                 test_file,
                 rare_feature_cutoff,
                 n_epochs,
                 comp_in_file,
                 comp_out_file)

    input_file.close()
    test_file.close()
    if comp_in_file:
        comp_in_file.close()
    if comp_out_file:
        comp_out_file.close()
    print("Finished\n")

#! /usr/bin/python

import numpy as np
from scipy import sparse
import time
import timeit
import itertools
from datetime import datetime
import parser_utils
import edmonds
import edmonds_fast

__author__ = "Igor Margulis, Nahum Farchi"

"""
A dependency parser uses GLM model to find the most likely paring tree
for a given sentence. The model is provided the feature extractor which
eventually creates embedding in the space where the learning takes place.

The learning procedure encodes these features and performs learning
in order to obtain the best parameter vector provided the data.
The underlying learning scheme used for training is Structured
Perceptron and the decoding (inference) is performed using Edmonds algorithm
for finding MST.
"""

class Glm(object):
    """
    Global Linear Model.

    """
    def __init__(self, feat_extractor, weights):
        """
        Construct a new GLM.  Typically, new GLM is created by
         ``train()``.

        @type feat_extractor: GlmFeatExtractor
        @param feat_extractor:  The extractor which performs the actual
        feature extraction from training data and encoding (embedding)
        into $d$-dimensional space.

        @type weights: list(float)
        @param weights:  The feature weight vector.
        """
        self._feat_extractor = feat_extractor
        self._v = weights

    def feat_extractor(self):
        return self._feat_extractor

    def set_weights(self, weights):
        self._v = weights

    def weights(self):
        return self._v

    def __repr__(self):
        return ('<GLM>: %d feature space dimension>' %
                ( self._feat_extractor.dim()))

    @classmethod
    def train(cls, training_sents, feat_cutoff, basic, n_epochs=10, verbose=False):
        """
        Learn the model based on the given corpus of training tagged sentences.

        @type training_sents: list('str','str')
        @param training_sents: the data

        @type feat_cutoff: int
        @param feat_cutoff: threshold for rear features.

        @type basic: bool
        @param basic: use basic model if True.

        @type basic: bool
        @param basic: use basic model if True.

        @type verbose: bool
        @param verbose: produce the output if True.

        """
        if verbose:
            t_start = time.time()
            print ("Feature extraction start time: {}".format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))

        feat_extractor = parser_utils.GlmFeatExtractor(training_sents, basic, feat_cutoff)

        if verbose:
            t_end = time.time()
            print("Feature extraction time in minutes: {}".format(round(t_end - t_start, 3)/60.))

        return train_with_perceptron(feat_extractor, n_epochs=n_epochs, verbose=verbose)

def train_with_perceptron(feat_extractor,  n_epochs=10, verbose=False, print_epoch_acc=False):
    """
    Train a new model, using the given training examples
    Employ Structured Perceptron algorithm for learning.
    """

    # Start with random weights
    weights = np.random.rand(feat_extractor.dim(), 1).astype(np.float64)
    weights_avg = np.copy(weights)

    if verbose:
        print('==> Training ==> Preliminary activity\n')
        print('dimension: {}'.format(feat_extractor.dim()))
        print('number of epochs: {}'.format(n_epochs))

    complete_feature_mat = feat_extractor.build_feature_matrix(feat_extractor.train_sents, verbose)
    true_feature_trees = feat_extractor.build_trees(feat_extractor.train_sents, verbose)
    complete_feature_graphs = feat_extractor.build_graphs(feat_extractor.train_sents, verbose)

    n_words = 0
    for sent in feat_extractor.train_sents:
        n_words += len(sent)

    if verbose:
        print('==> Training ==> Starting learning\n')

    # Estimate Perceptron weights
    for n in xrange(n_epochs):
        if verbose:
            t_epoc_start = timeit.default_timer()
            print('epoch {}'.format(n))

        for t in np.random.permutation(len(complete_feature_graphs)):
            feature_g = complete_feature_graphs[t]
            if verbose and t % parser_utils.PRINT_EPOCH == 0:
                print("t: %d" % t)
            feature_pred_tree = predict(complete_feature_mat, feature_g, weights)
            feature_true_tree = true_feature_trees[t]

            if not parser_utils.compare_graphs(feature_true_tree, feature_pred_tree):
                f_pred = sentence_feature_vect(complete_feature_mat, feature_pred_tree)
                f_true = sentence_feature_vect(complete_feature_mat, feature_true_tree)
                weights = weights + f_true.T - f_pred.T

        weights_avg = weights_avg + weights

        if verbose and print_epoch_acc:
            epoch_acc = 0.0
            for t, feature_g in enumerate(complete_feature_graphs):
                feature_pred_tree = predict(complete_feature_mat, feature_g, weights)
                feature_true_tree = true_feature_trees[t]
                epoch_acc += parser_utils.compare_graphs_count(feature_true_tree, feature_pred_tree)
            epoch_acc = 1.0 - epoch_acc / n_words
            print("Epoch acc: {}".format(epoch_acc))

        if verbose:
            t_epoch_end = timeit.default_timer()
            print("Epoch time in minutes: {}".format(round(t_epoch_end - t_epoc_start, 3) / 60.))

    weights_avg = weights_avg / n_epochs

    if verbose:
        epoch_acc = 0.0
        for t, feature_g in enumerate(complete_feature_graphs):
            feature_pred_tree = predict(complete_feature_mat, feature_g, weights_avg)
            feature_true_tree = true_feature_trees[t]
            epoch_acc += parser_utils.compare_graphs_count(feature_true_tree, feature_pred_tree)
        epoch_acc = 1.0 - epoch_acc / n_words
        print("Final Epoch acc: {}".format(epoch_acc))
        print('==> Training ==> Done\n')

    # Return the model.
    return Glm(feat_extractor, weights_avg)


def sentence_feature_vect(complete_feature_mat, feature_g):
    """
    Sum all feature vectors that are on the edges and return the result.
    :param feature_g:
    :param dim:
    :return:
    """
    rows = [feature_g[src][dst] for src in feature_g for dst in feature_g[src]]
    return sparse.csr_matrix.sum(complete_feature_mat[rows, :], axis=0)


def calc_graph_weights(complete_feature_mat, feature_g, weights):

    rows = [feature_g[src][dst] for src in feature_g for dst in feature_g[src]]
    edge_weights = -complete_feature_mat[rows, :].dot(weights)

    weighted_g = {}
    r = itertools.count()
    for src in feature_g:
        for dst in feature_g[src]:
            if src in weighted_g:
                weighted_g[src][dst] = edge_weights[next(r)].flat[0]
            else:
                weighted_g[src] = {dst: edge_weights[next(r)].flat[0]}
    return weighted_g

# Make a prediction with weights (i.e., find parse tree)
ROOT = 0


def predict(complete_feature_mat, feature_g, weights):
    """
    Predict the parse tree using edmonds mst algorithm.
    Return a tree with feature vectors on the edges and
    a tree with score weights on the edges.

    :param feature_g:
    :param weights:
    :return:
    """
    weighted_g = calc_graph_weights(complete_feature_mat, feature_g, weights)
    weighted_tree = edmonds.mst(ROOT, weighted_g)

    feature_tree = {}
    for src in weighted_tree:
        for dst in weighted_tree[src]:
            if src in feature_tree:
                feature_tree[src][dst] = feature_g[src][dst]
            else:
                feature_tree[src] = {dst : feature_g[src][dst]}
    return feature_tree

# NLP-Dependency-Parser
## Depdendencies
- Python 2.7.13
- enum (run pip install --upgrade pip enum34 if there's an import error)
- Numpy
- Scipy

## Usage
    python dep_parser.py  [-bvn<n_epocs>r<feature_cutoff>] -i <training.file> -t <test.file> [-c <input.file>]
    
Train the Dependency Parser and test it.

    -h                -- display help
    -b                -- use the basic model
    -v                -- verbose
    -i training.file  -- path to the file containing training data
    -t test.file      -- path to the file containing test data
    -c input.file     --  path to the file containing unlabeled data
    -n n_epochs       -- number of perceptron epochs
    -o out.file       -- output file of unlabled labeling
    -r feature_cutoff -- feature cutoff threshold
    
For example, to run with *basic features*:

    python dep_parser.py -i train.labeled -t test.labeled -c comp.unlabeled -o comp.labeled -n 30 -v -r 0 -b
    
or, to run with *advanced features*:

    python dep_parser.py -i train.labeled -t test.labeled -c comp.unlabeled -o comp.labeled -n 30 -v -r 0

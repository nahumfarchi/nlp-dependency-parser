python ../dep_parser.py -i ../train.labeled -t ../test.labeled -v -b -n 20 > log/basic_20.txt
python ../dep_parser.py -i ../train.labeled -t ../test.labeled -v -b -n 50 > log/basic_50.txt
python ../dep_parser.py -i ../train.labeled -t ../test.labeled -v -b -n 80 > log/basic_80.txt
python ../dep_parser.py -i ../train.labeled -t ../test.labeled -v -b -n 100 > log/basic_100.txt

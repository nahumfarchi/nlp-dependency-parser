python ../dep_parser.py -i ../train.labeled -t ../test.labeled -v -n 20 -r 2 > log/advanced_e20_r2.txt
python ../dep_parser.py -i ../train.labeled -t ../test.labeled -v -n 50 -r 2 > log/advanced_e50_r2.txt
python ../dep_parser.py -i ../train.labeled -t ../test.labeled -v -n 80 -r 2 > log/advanced_e80_r2.txt
python ../dep_parser.py -i ../train.labeled -t ../test.labeled -v -n 100 -r 2 > log/advanced_e100_r2.txt
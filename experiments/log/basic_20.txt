Started

Start time: 2017-01-25 22:25:43
Feature extraction start time: 2017-01-25 22:25:43
Feature extraction time in minutes: 0.0357166666667
==> Training ==> Pre-optimization activity

dimension: 81084
number of epochs: 20
Building feature matrix...
i: 0
i: 1000
i: 2000
i: 3000
i: 4000
time: 1.01852774157
Building trees...
i: 0
i: 1000
i: 2000
i: 3000
i: 4000
Time: 0.0174103910004
Building graphs...
i: 0
i: 1000
i: 2000
i: 3000
i: 4000
time: 0.0236846032919
==> Training ==> Starting learning

Epoch 0, start time: 2017-01-25 22:26:49
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.15108333333
Epoch acc: 0.263740731882
Epoch 1, start time: 2017-01-25 22:27:58
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.26946666667
Epoch acc: 0.280499083154
Epoch 2, start time: 2017-01-25 22:29:14
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.28493333333
Epoch acc: 0.304424778761
Epoch 3, start time: 2017-01-25 22:30:31
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.30946666667
Epoch acc: 0.289324722953
Epoch 4, start time: 2017-01-25 22:31:50
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.23743333333
Epoch acc: 0.304121820936
Epoch 5, start time: 2017-01-25 22:33:04
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.05585
Epoch acc: 0.312333572511
Epoch 6, start time: 2017-01-25 22:34:08
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.04858333333
Epoch acc: 0.301267639321
Epoch 7, start time: 2017-01-25 22:35:11
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.04573333333
Epoch acc: 0.318488399904
Epoch 8, start time: 2017-01-25 22:36:13
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.05536666667
Epoch acc: 0.322984931834
Epoch 9, start time: 2017-01-25 22:37:17
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.08823333333
Epoch acc: 0.319500916846
Epoch 10, start time: 2017-01-25 22:38:22
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.06663333333
Epoch acc: 0.337208004465
Epoch 11, start time: 2017-01-25 22:39:26
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.0697
Epoch acc: 0.342222753727
Epoch 12, start time: 2017-01-25 22:40:30
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.04315
Epoch acc: 0.323184246193
Epoch 13, start time: 2017-01-25 22:41:33
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.04361666667
Epoch acc: 0.325488320179
Epoch 14, start time: 2017-01-25 22:42:35
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.04995
Epoch acc: 0.31172765686
Epoch 15, start time: 2017-01-25 22:43:38
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.04031666667
Epoch acc: 0.317125089691
Epoch 16, start time: 2017-01-25 22:44:41
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.0323
Epoch acc: 0.3291796221
Epoch 17, start time: 2017-01-25 22:45:43
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.03286666667
Epoch acc: 0.327170533365
Epoch 18, start time: 2017-01-25 22:46:45
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.04081666667
Epoch acc: 0.333939248983
Epoch 19, start time: 2017-01-25 22:47:47
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 1.0301
Epoch acc: 0.343378777007
==> Training ==> Done

Training time in minutes: 23.09485
==> Evaluation ===> started at: 2017-01-25 22:48:49
==> Evaluation ===> ended at: 2017-01-25 22:49:06
Evaluation time in minutes: 0.27445
==> Evaluation ===> Parser accuracy 28.0513326752%




Finished


Started

Start time: 2017-01-26 15:21:07
Feature extraction start time: 2017-01-26 15:21:07
Feature extraction time in minutes: 0.0936333333333
==> Training ==> Pre-optimization activity

dimension: 363215
number of epochs: 80
Building feature matrix...
i: 0
i: 1000
i: 2000
i: 3000
i: 4000
time: 1.80681736733
Building trees...
i: 0
i: 1000
i: 2000
i: 3000
i: 4000
Time: 0.0165290906727
Building graphs...
i: 0
i: 1000
i: 2000
i: 3000
i: 4000
time: 0.0356675065773
==> Training ==> Starting learning

Epoch 0, start time: 2017-01-26 15:23:04
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.1529
Epoch acc: 0.425647771665
Epoch 1, start time: 2017-01-26 15:26:13
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.13761666667
Epoch acc: 0.478147173722
Epoch 2, start time: 2017-01-26 15:29:21
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.18198333333
Epoch acc: 0.563286295145
Epoch 3, start time: 2017-01-26 15:32:32
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.1171
Epoch acc: 0.588854341067
Epoch 4, start time: 2017-01-26 15:35:39
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.08203333333
Epoch acc: 0.614757235111
Epoch 5, start time: 2017-01-26 15:38:44
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.07301666667
Epoch acc: 0.655600733477
Epoch 6, start time: 2017-01-26 15:41:49
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.12776666667
Epoch acc: 0.650275053815
Epoch 7, start time: 2017-01-26 15:44:56
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.06473333333
Epoch acc: 0.664019771984
Epoch 8, start time: 2017-01-26 15:48:00
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.05268333333
Epoch acc: 0.672231523559
Epoch 9, start time: 2017-01-26 15:51:03
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.08285
Epoch acc: 0.686024077175
Epoch 10, start time: 2017-01-26 15:54:08
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.05318333333
Epoch acc: 0.679111855218
Epoch 11, start time: 2017-01-26 15:57:12
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.0554
Epoch acc: 0.677708682133
Epoch 12, start time: 2017-01-26 16:00:15
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.07568333333
Epoch acc: 0.690026309495
Epoch 13, start time: 2017-01-26 16:03:19
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.04003333333
Epoch acc: 0.696205054612
Epoch 14, start time: 2017-01-26 16:06:22
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.24865
Epoch acc: 0.696699354221
Epoch 15, start time: 2017-01-26 16:09:37
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.97675
Epoch acc: 0.70772542454
Epoch 16, start time: 2017-01-26 16:13:35
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 4.86631666667
Epoch acc: 0.689229052061
Epoch 17, start time: 2017-01-26 16:18:27
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.4903
Epoch acc: 0.702981742805
Epoch 18, start time: 2017-01-26 16:21:57
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.73403333333
Epoch acc: 0.694211911026
Epoch 19, start time: 2017-01-26 16:25:41
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.66258333333
Epoch acc: 0.697042174918
Epoch 20, start time: 2017-01-26 16:29:20
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.98526666667
Epoch acc: 0.701355337639
Epoch 21, start time: 2017-01-26 16:33:20
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.42155
Epoch acc: 0.707087618592
Epoch 22, start time: 2017-01-26 16:36:45
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 3.59503333333
Epoch acc: 0.691022881288
Epoch 23, start time: 2017-01-26 16:40:21
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 4.98066666667
Epoch acc: 0.70990990991
Epoch 24, start time: 2017-01-26 16:45:19
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 5.3373
Epoch acc: 0.704759626884
Epoch 25, start time: 2017-01-26 16:50:40
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 5.57183333333
Epoch acc: 0.706768715618
Epoch 26, start time: 2017-01-26 16:56:14
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 5.45388333333
Epoch acc: 0.711998724388
Epoch 27, start time: 2017-01-26 17:01:41
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 5.63658333333
Epoch acc: 0.71853623535
Epoch 28, start time: 2017-01-26 17:07:19
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 5.99311666667
Epoch acc: 0.706832496213
Epoch 29, start time: 2017-01-26 17:13:19
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 5.91901666667
Epoch acc: 0.708084190385
Epoch 30, start time: 2017-01-26 17:19:14
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 4.60371666667
Epoch acc: 0.709383720003
Epoch 31, start time: 2017-01-26 17:23:50
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 5.3745
Epoch acc: 0.69768795344
Epoch 32, start time: 2017-01-26 17:29:13
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 5.9895
Epoch acc: 0.704408833612
Epoch 33, start time: 2017-01-26 17:35:12
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 5.477
Epoch acc: 0.703252810332
Epoch 34, start time: 2017-01-26 17:40:41
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 5.50983333333
Epoch acc: 0.714733317388
Epoch 35, start time: 2017-01-26 17:46:11
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 5.90911666667
Epoch acc: 0.704448696484
Epoch 36, start time: 2017-01-26 17:52:06
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 5.90946666667
Epoch acc: 0.704871242924
Epoch 37, start time: 2017-01-26 17:58:01
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 5.37223333333
Epoch acc: 0.720625049829
Epoch 38, start time: 2017-01-26 18:03:23
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 5.8295
Epoch acc: 0.721183130033
Epoch 39, start time: 2017-01-26 18:09:13
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 6.32666666667
Epoch acc: 0.714366578968
Epoch 40, start time: 2017-01-26 18:15:32
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 7.16863333333
Epoch acc: 0.705166228175
Epoch 41, start time: 2017-01-26 18:22:42
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 5.657
Epoch acc: 0.724085147094
Epoch 42, start time: 2017-01-26 18:28:22
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 5.8324
Epoch acc: 0.706378059475
Epoch 43, start time: 2017-01-26 18:34:12
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 6.73993333333
Epoch acc: 0.724507693534
Epoch 44, start time: 2017-01-26 18:40:56
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 7.4497
Epoch acc: 0.719923463286
Epoch 45, start time: 2017-01-26 18:48:23
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 7.95083333333
Epoch acc: 0.697105955513
Epoch 46, start time: 2017-01-26 18:56:20
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 6.74695
Epoch acc: 0.72418879056
Epoch 47, start time: 2017-01-26 19:03:05
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 6.21071666667
Epoch acc: 0.719038507534
Epoch 48, start time: 2017-01-26 19:09:18
t: 0
t: 1000
t: 2000
t: 3000
t: 4000
Epoch time in minutes: 7.772
Epoch acc: 0.707286932951
Epoch 49, start time: 2017-01-26 19:17:04
t: 0
t: 1000
t: 2000
t: 3000
